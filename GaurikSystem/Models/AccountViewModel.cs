﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GaurikSystem.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class ContactUsModel
    {
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]

        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }
        [Required]
        public string Message { get; set; }

    }
    public class QuoteModel
    {
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]

        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",  ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }
        [Display(Name = "Mobile")]
        [Required]
        public string Mobile { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        [Display(Name = "Service")]
        public string Services { get; set; }
        [Required]
        [Display(Name = "Start time")]
        public DateTime? Starttime { get; set; }
        [Required]
        [Display(Name = "Webite Type")]
        public string WebiteType { get; set; }
        [Required(ErrorMessage = "This field are required")]
        public bool? IsPersonalOwnership { get; set; }
        [Required(ErrorMessage = "This field are required")]
        public bool? DomainRequired{get;set;}
        [Required(ErrorMessage = "This field are required")]
        public bool? HostingRequired { get; set; }
        [Required]
        public string Address { get; set; }

    }
}