﻿using System.Collections.Generic;
using System.Linq;

namespace GaurikSystem.Models
{
    
        /// <summary>
        /// Process payment result
        /// </summary>
        public partial class ProcessPaymentResult
        {
            private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;

            /// <summary>
            /// Ctor
            /// </summary>
            public ProcessPaymentResult()
            {
                this.Errors = new List<string>();
            }

            /// <summary>
            /// Gets a value indicating whether request has been completed successfully
            /// </summary>
            public bool Success
            {
                get { return (this.Errors.Count == 0); }
            }

            /// <summary>
            /// Add error
            /// </summary>
            /// <param name="error">Error</param>
            public void AddError(string error)
            {
                this.Errors.Add(error);
            }

            /// <summary>
            /// Errors
            /// </summary>
            public IList<string> Errors { get; set; }


            /// <summary>
            /// Gets or sets an AVS result
            /// </summary>
            public string AvsResult { get; set; }

            /// <summary>
            /// Gets or sets the authorization transaction identifier
            /// </summary>
            public string AuthorizationTransactionId { get; set; }

            /// <summary>
            /// Gets or sets the authorization transaction code
            /// </summary>
            public string AuthorizationTransactionCode { get; set; }

            /// <summary>
            /// Gets or sets the authorization transaction result
            /// </summary>
            public string AuthorizationTransactionResult { get; set; }

            /// <summary>
            /// Gets or sets the capture transaction identifier
            /// </summary>
            public string CaptureTransactionId { get; set; }

            /// <summary>
            /// Gets or sets the capture transaction result
            /// </summary>
            public string CaptureTransactionResult { get; set; }

            /// <summary>
            /// Gets or sets the subscription transaction identifier
            /// </summary>
            public string SubscriptionTransactionId { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether storing of credit card number, CVV2 is allowed
            /// </summary>
            public bool AllowStoringCreditCardNumber { get; set; }

            public PaymentStatus NewPaymentStatus
            {
                get
                {
                    return _newPaymentStatus;
                }
                set
                {
                    _newPaymentStatus = value;
                }
            }



        }

        public class RefundPaymentRequest
        {
            public string CaptureTransactionId { get; set; }
            public string AuthorizationTransactionCode { get; set; }
            public string MaskedCreditCardNumber { get; set; }
            public decimal AmountToRefund { get; set; }
            public decimal RefundedAmount { get; set; }
            public decimal CheckOutPrice { get; set; }
            public string PaymentGUID { get; set; }
        }

        /// <summary>
        /// Refund payment result
        /// </summary>
        public partial class RefundPaymentResult
        {
            private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;

            /// <summary>
            /// Ctor
            /// </summary>
            public RefundPaymentResult()
            {
                this.Errors = new List<string>();
            }

            /// <summary>
            /// Gets a value indicating whether request has been completed successfully
            /// </summary>
            public bool Success
            {
                get { return (this.Errors.Count == 0); }
            }

            /// <summary>
            /// Add error
            /// </summary>
            /// <param name="error">Error</param>
            public void AddError(string error)
            {
                this.Errors.Add(error);
            }

            /// <summary>
            /// Errors
            /// </summary>
            public IList<string> Errors { get; set; }

            /// <summary>
            /// Gets or sets a payment status after processing
            /// </summary>
            public PaymentStatus NewPaymentStatus
            {
                get
                {
                    return _newPaymentStatus;
                }
                set
                {
                    _newPaymentStatus = value;
                }
            }
        }

        public partial class CapturePaymentResult
        {
            private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;

            /// <summary>
            /// Ctor
            /// </summary>
            public CapturePaymentResult()
            {
                this.Errors = new List<string>();
            }

            /// <summary>
            /// Gets a value indicating whether request has been completed successfully
            /// </summary>
            public bool Success
            {
                get { return (!this.Errors.Any()); }
            }

            /// <summary>
            /// Add error
            /// </summary>
            /// <param name="error">Error</param>
            public void AddError(string error)
            {
                this.Errors.Add(error);
            }

            /// <summary>
            /// Errors
            /// </summary>
            public IList<string> Errors { get; set; }

            /// <summary>
            /// Gets or sets the capture transaction identifier
            /// </summary>
            public string CaptureTransactionId { get; set; }

            /// <summary>
            /// Gets or sets the capture transaction result
            /// </summary>
            public string CaptureTransactionResult { get; set; }

            /// <summary>
            /// Gets or sets a payment status after processing
            /// </summary>
            public PaymentStatus NewPaymentStatus
            {
                get
                {
                    return _newPaymentStatus;
                }
                set
                {
                    _newPaymentStatus = value;
                }
            }
        }

        /// <summary>
        /// Represents a VoidPaymentResult
        /// </summary>
        public partial class VoidPaymentResult
        {
            private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;

            /// <summary>
            /// Ctor
            /// </summary>
            public VoidPaymentResult()
            {
                this.Errors = new List<string>();
            }

            /// <summary>
            /// Gets a value indicating whether request has been completed successfully
            /// </summary>
            public bool Success
            {
                get { return (!this.Errors.Any()); }
            }

            /// <summary>
            /// Add error
            /// </summary>
            /// <param name="error">Error</param>
            public void AddError(string error)
            {
                this.Errors.Add(error);
            }

            /// <summary>
            /// Errors
            /// </summary>
            public IList<string> Errors { get; set; }

            /// <summary>
            /// Gets or sets a payment status after processing
            /// </summary>
            public PaymentStatus NewPaymentStatus
            {
                get
                {
                    return _newPaymentStatus;
                }
                set
                {
                    _newPaymentStatus = value;
                }
            }
        }

        /// <summary>
        /// Represents a payment status enumeration
        /// </summary>
        public enum PaymentStatus
        {
            Paid = 1,
            Pending = 2,
            Voided = 3,
            Authorized = 4,
            PartiallyRefunded = 5,
            Refunded = 6,
            Cancel = 7
        }
   
}