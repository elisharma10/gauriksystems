﻿using System.ComponentModel.DataAnnotations;

namespace GaurikSystem.Models
{
    public class BillPaymentCardDetailModel
    {
        [Required]
        [Display(Name = "Card Number")]
        public string CreditCardNumber { get; set; }
        [Required]
        [Display(Name = "Credit Card Expire Year")]
        public string CreditCardExpireYear { get; set; }
        [Required]
        [Display(Name = "Credit Card Expire Month")]
        public string CreditCardExpireMonth { get; set; }
        [Required]
        [Display(Name = "Cvv2")]
        public string CreditCardCvv2 { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string ZipPostalCode { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        [Display(Name = "State")]
        public string CountryTwoLetterIsoCode { get; set; }
        [Display(Name = "State")]
        public string StateProvince { get; set; }
        [Required]
        [Display(Name = "State")]
        public string StateProvinceAbbreviation { get; set; }
        public string OrderGuid { get; set; }
        [Display(Name= "Payment amount")]
        public decimal TotalAmount { get; set; }
        public string CaptureTransactionId { get; set; }
        public string AuthorizationTransactionCode { get; set; }
        public string MaskedCreditCardNumber { get; set; }
        public decimal? RefundedAmount { get; set; }
        public int PaymentStatusId { get; set; }
       // public string Title { get; set; }
       // public string OrganizationName { get; set; }
       
    }
}