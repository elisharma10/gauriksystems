﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace GaurikSystem.Helper
{
    public class EmailHelper
    {
        public static bool SendEmail(string toEmail, string subject, string body,string fromName,string fromEmail, string cc="", string bcc="")
        {
            var message = new MailMessage();
            var objSmtpDetails = GetSmtpDetails();
            try
            {
                var fromAddress = new MailAddress(fromEmail,fromName);
                message.From = fromAddress;
                message.To.Add(toEmail);
                message.Subject = subject;
                if (cc != null && Utilities.IsValidEmailAddressByRegex(cc))
                    message.CC.Add(cc);
                if (bcc != null && Utilities.IsValidEmailAddressByRegex(bcc))
                    message.Bcc.Add(bcc);
                message.IsBodyHtml = true;
                message.Body = body;
                // Send SMTP mail
                var smtp = new SmtpClient(objSmtpDetails.SmtpServer)
                {
                    Credentials = new NetworkCredential(objSmtpDetails.UserName, objSmtpDetails.Password),
                    Port = objSmtpDetails.PortNO,
                    EnableSsl = objSmtpDetails.EnableSsl
                };
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                //log error
                return false;
            }
        }

        public class SmtpDetails
        {
            public bool EnableSsl { get; set; }
            public int PortNO { get; set; }
            public string SmtpServer { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }


        private static SmtpDetails GetSmtpDetails()
        {
            var objAppSettingsReader = new AppSettingsReader();
            var objSmtpDetails = new SmtpDetails();
            objSmtpDetails.EnableSsl = Convert.ToBoolean(objAppSettingsReader.GetValue("EnableSsl", typeof(string)));
            objSmtpDetails.UserName = objAppSettingsReader.GetValue("NotificationEmail", typeof(string)).ToString();
            objSmtpDetails.Password = objAppSettingsReader.GetValue("NotificationPassword", typeof(string)).ToString();
            objSmtpDetails.PortNO = Convert.ToInt32(objAppSettingsReader.GetValue("SmtpPort", typeof(string)));
            objSmtpDetails.SmtpServer = objAppSettingsReader.GetValue("SmtpServer", typeof(string)).ToString();
            return objSmtpDetails;
        }
    }
}