﻿using AuthorizeNetSDK = AuthorizeNet;
using AuthorizeNet.Api.Controllers.Bases;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using GaurikSystem.Models;
using GaurikSystem.Helper;

namespace GaurikSystem.Provider.AuthorizeNet
{
    public class AuthorizeNetPaymentProcessor
    {
        #region Fields
        private readonly AuthorizeNetPaymentSettings _authorizeNetPaymentSettings;
        #endregion

        #region Ctor

        public AuthorizeNetPaymentProcessor(TransactMode transactMode = TransactMode.AuthorizeAndCapture)
        {
            _authorizeNetPaymentSettings = new AuthorizeNetPaymentSettings
            {
                TransactMode = transactMode
            };
        }

        #endregion
        private void PrepareAuthorizeNet()
        {

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = ConfigurationManager.AppSettings["AuthorizeNetUseSandbox"] == "true"
                ? AuthorizeNetSDK.Environment.SANDBOX
                : AuthorizeNetSDK.Environment.PRODUCTION;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetLoginId"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"]
            };

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        //public static ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest, Address billingaddress)
        public ProcessPaymentResult ProcessPayment(BillPaymentCardDetailModel processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            PrepareAuthorizeNet();
            var creditCard = new creditCardType
            {
                cardNumber = processPaymentRequest.CreditCardNumber,//EncryptionHelper.Decrypt(processPaymentRequest.CreditCardNumber),
                expirationDate =
                  Convert.ToInt32(processPaymentRequest.CreditCardExpireMonth).ToString("D2") + processPaymentRequest.CreditCardExpireYear,
                //Convert.ToInt32(EncryptionHelper.Decrypt(processPaymentRequest.CreditCardExpireMonth)).ToString("D2") + EncryptionHelper.Decrypt(processPaymentRequest.CreditCardExpireYear),
                cardCode = processPaymentRequest.CreditCardCvv2
                //EncryptionHelper.Decrypt(processPaymentRequest.CreditCardCvv2)
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };
            transactionTypeEnum transactionType;

            switch (_authorizeNetPaymentSettings.TransactMode)
            {
                case TransactMode.Authorize:
                    transactionType = transactionTypeEnum.authOnlyTransaction;
                    break;
                case TransactMode.AuthorizeAndCapture:
                    transactionType = transactionTypeEnum.authCaptureTransaction;
                    break;
                default:
                    throw new Exception("Not supported transaction mode");
            }

            var billTo = new customerAddressType
            {
                firstName = processPaymentRequest.FirstName,
                lastName = processPaymentRequest.LastName,
                email = processPaymentRequest.Email,
                address = processPaymentRequest.Address1,
                city = processPaymentRequest.City,
                zip = processPaymentRequest.ZipPostalCode
            };
            if (!string.IsNullOrEmpty(processPaymentRequest.Company))
                billTo.company = processPaymentRequest.Company;
            if (processPaymentRequest.StateProvince != null)
                billTo.state = processPaymentRequest.StateProvinceAbbreviation;
            if (processPaymentRequest.Country != null)
                billTo.country = processPaymentRequest.CountryTwoLetterIsoCode;
            //billTo.country = customer.BillingAddress.Country.TwoLetterIsoCode;

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionType.ToString(),
                amount = Math.Round(processPaymentRequest.TotalAmount, 2),
                payment = paymentType,
                currencyCode = "USD",
                billTo = billTo,
                customerIP = Utilities.GetCurrentIpAddress(),
                order = new orderType
                {
                    //x_invoice_num is 20 chars maximum. hece we also pass x_description
                    invoiceNumber = processPaymentRequest.OrderGuid.ToString().Substring(0, 20),
                    description = string.Format("Full order #{0}", processPaymentRequest.OrderGuid)
                }
            };
            var request = new createTransactionRequest { transactionRequest = transactionRequest };
            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();
            // get the response from the service (errors contained if any)
            var response = GetApiResponse(controller, result.Errors);
            //validate
            if (response == null)
                return result;
            if (_authorizeNetPaymentSettings.TransactMode == TransactMode.Authorize)
            {
                result.AuthorizationTransactionId = response.transactionResponse.transId;
                result.AuthorizationTransactionCode = string.Format("{0},{1}", response.transactionResponse.transId, response.transactionResponse.authCode);
            }
            if (_authorizeNetPaymentSettings.TransactMode == TransactMode.AuthorizeAndCapture)
            {
                result.CaptureTransactionId = string.Format("{0},{1}", response.transactionResponse.transId, response.transactionResponse.authCode);
            }
            result.AuthorizationTransactionResult = string.Format("Approved ({0}: {1})", response.transactionResponse.responseCode, response.transactionResponse.messages[0].description);
            result.AvsResult = response.transactionResponse.avsResultCode;
            result.NewPaymentStatus = _authorizeNetPaymentSettings.TransactMode == TransactMode.Authorize ? PaymentStatus.Authorized : PaymentStatus.Paid;

            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(BillPaymentCardDetailModel capturePaymentRequest)
        {
            var result = new CapturePaymentResult();

            PrepareAuthorizeNet();

            var codes = capturePaymentRequest.AuthorizationTransactionCode.Split(',');
            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.priorAuthCaptureTransaction.ToString(),
                amount = Math.Round(capturePaymentRequest.TotalAmount, 2),
                refTransId = codes[0],
                currencyCode = "USD",
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();
            // get the response from the service (errors contained if any)
            var response = GetApiResponse(controller, result.Errors);
            //validate
            if (response == null)
                return result;
            result.CaptureTransactionId = string.Format("{0},{1}", response.transactionResponse.transId, response.transactionResponse.authCode);
            result.CaptureTransactionResult = string.Format("Approved ({0}: {1})", response.transactionResponse.responseCode, response.transactionResponse.messages[0].description);
            result.NewPaymentStatus = PaymentStatus.Paid;
            return result;
        }


        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            PrepareAuthorizeNet();

            var maskedCreditCardNumberDecrypted = refundPaymentRequest.MaskedCreditCardNumber;

            if (String.IsNullOrEmpty(maskedCreditCardNumberDecrypted) || maskedCreditCardNumberDecrypted.Length < 4)
            {
                result.AddError("Last four digits of Credit Card Not Available");
                return result;
            }

            var lastFourDigitsCardNumber = maskedCreditCardNumberDecrypted.Substring(maskedCreditCardNumberDecrypted.Length - 4);
            var creditCard = new creditCardType
            {
                cardNumber = lastFourDigitsCardNumber,
                expirationDate = "XXXX"
                //cardNumber = lastFourDigitsCardNumber,
                //expirationDate = "XXXX"
            };

            var codes = (string.IsNullOrEmpty(refundPaymentRequest.CaptureTransactionId) ? refundPaymentRequest.AuthorizationTransactionCode : refundPaymentRequest.CaptureTransactionId).Split(',');

            //check transaction status
            var transactionDetail = GetTransactionDetail(codes[0]);

            if (transactionDetail.transaction.transactionStatus != "settledSuccessfully")
            {
                // void transaction
                return this.Void(refundPaymentRequest);
            }
            else
            {
                var transactionRequest = new transactionRequestType
                {
                    transactionType = transactionTypeEnum.refundTransaction.ToString(),
                    amount = Math.Round(refundPaymentRequest.AmountToRefund, 2),
                    refTransId = codes[0],
                    currencyCode = "USD",
                    order = new orderType
                    {
                        //x_invoice_num is 20 chars maximum. hece we also pass x_description
                        invoiceNumber = refundPaymentRequest.PaymentGUID.ToString().Substring(0, 20),
                        description = string.Format("Full order #{0}", refundPaymentRequest.PaymentGUID)
                    },

                    payment = new paymentType { Item = creditCard }
                };

                var request = new createTransactionRequest { transactionRequest = transactionRequest };

                // instantiate the contoller that will call the service
                var controller = new createTransactionController(request);
                controller.Execute();

                var response = GetApiResponse(controller, result.Errors);

                //validate
                if (response == null)
                    return result;

                var isOrderFullyRefunded = refundPaymentRequest.AmountToRefund + refundPaymentRequest.RefundedAmount == refundPaymentRequest.CheckOutPrice;
                result.NewPaymentStatus = isOrderFullyRefunded ? PaymentStatus.Refunded : PaymentStatus.PartiallyRefunded;

                return result;
            }

        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Void(RefundPaymentRequest voidPaymentRequest)
        {
            var result = new RefundPaymentResult();

            PrepareAuthorizeNet();

            var maskedCreditCardNumberDecrypted = voidPaymentRequest.MaskedCreditCardNumber;

            if (String.IsNullOrEmpty(maskedCreditCardNumberDecrypted) || maskedCreditCardNumberDecrypted.Length < 4)
            {
                result.AddError("Last four digits of Credit Card Not Available");
                return result;
            }

            var lastFourDigitsCardNumber = maskedCreditCardNumberDecrypted.Substring(maskedCreditCardNumberDecrypted.Length - 4);
            //var expirationDate = voidPaymentRequest.CreditCardExpireMonth + voidPaymentRequest.CreditCardExpireYear;

            //if (!expirationDate.Any())
            var expirationDate = "XXXX";

            var creditCard = new creditCardType
            {
                cardNumber = lastFourDigitsCardNumber,
                expirationDate = expirationDate
            };

            var codes = (string.IsNullOrEmpty(voidPaymentRequest.CaptureTransactionId) ? voidPaymentRequest.AuthorizationTransactionCode : voidPaymentRequest.CaptureTransactionId).Split(',');
            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.voidTransaction.ToString(),
                refTransId = codes[0],
                payment = new paymentType { Item = creditCard }
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            var response = GetApiResponse(controller, result.Errors);

            //validate
            if (response == null)
                return result;

            result.NewPaymentStatus = PaymentStatus.Voided;

            return result;
        }




        private createTransactionResponse GetApiResponse(createTransactionController controller, IList<string> errors)
        {
            var response = controller.GetApiResponse();

            if (response != null)
            {
                if (response.transactionResponse != null && response.transactionResponse.errors != null)
                {
                    foreach (var transactionResponseError in response.transactionResponse.errors)
                    {
                        errors.Add(string.Format("Error #{0}: {1}", transactionResponseError.errorCode,
                            transactionResponseError.errorText));
                    }

                    return null;
                }

                if (response.transactionResponse != null && response.messages.resultCode == messageTypeEnum.Ok)
                {
                    switch (response.transactionResponse.responseCode)
                    {
                        case "1":
                            {
                                return response;
                            }
                        case "2":
                            {
                                var description = response.transactionResponse.messages.Any()
                                    ? response.transactionResponse.messages.First().description
                                    : String.Empty;
                                errors.Add(
                                    string.Format("Declined ({0}: {1})", response.transactionResponse.responseCode,
                                        description).TrimEnd(':', ' '));
                                return null;
                            }
                    }
                }
                else if (response.transactionResponse != null && response.messages.resultCode == messageTypeEnum.Error)
                {
                    if (response.messages != null && response.messages.message != null && response.messages.message.Any())
                    {
                        var message = response.messages.message.First();

                        errors.Add(string.Format("Error #{0}: {1}", message.code, message.text));
                        return null;
                    }
                }
            }
            else
            {
                var error = controller.GetErrorResponse();
                if (error != null && error.messages != null && error.messages.message != null && error.messages.message.Any())
                {
                    var message = error.messages.message.First();

                    errors.Add(string.Format("Error #{0}: {1}", message.code, message.text));
                    return null;
                }
            }
            var controllerResult = controller.GetResults().FirstOrDefault();
            const string unknownError = "Authorize.NET unknown error";
            errors.Add(String.IsNullOrEmpty(controllerResult) ? unknownError : String.Format("{0} ({1})", unknownError, controllerResult));
            return null;
        }


        public getTransactionDetailsResponse GetTransactionDetail(string transactionId)
        {
            PrepareAuthorizeNet();

            var request = new getTransactionDetailsRequest();
            request.transId = transactionId;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transaction == null)
                    return response;

                //Console.WriteLine("Transaction Id: {0}", response.transaction.transId);
                //Console.WriteLine("Transaction type: {0}", response.transaction.transactionType);
                //Console.WriteLine("Transaction status: {0}", response.transaction.transactionStatus);
                //Console.WriteLine("Transaction auth amount: {0}", response.transaction.authAmount);
                //Console.WriteLine("Transaction settle amount: {0}", response.transaction.settleAmount);
            }
            else if (response != null)
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
            }

            return response;
        }

    }
}