//Header Small on scroll

jQuery(function () {
    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 10) {
            jQuery("header").addClass('header-fixed-top');
        } else {
            jQuery("header").removeClass("header-fixed-top");
        }
    });
}); 

// Back to top button
jQuery(document).ready(function(){
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 50) {
            jQuery('#back-to-top').fadeIn();
        } else {
            jQuery('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    jQuery('#back-to-top').click(function () {
        jQuery('#back-to-top').tooltip('hide');
        jQuery('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    jQuery('#back-to-top').tooltip('show');

});

// Menu toggle in mobile view
jQuery(document).ready(function(){
    jQuery("#toggle-menu").click(function(){
        jQuery(".main-menu").slideToggle();
    });
});