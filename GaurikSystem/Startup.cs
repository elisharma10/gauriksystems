﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GaurikSystem.Startup))]
namespace GaurikSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
