﻿using GaurikSystem.Helper;
using GaurikSystem.Models;
using reCAPTCHA.MVC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GaurikSystem.Controllers
{
    public class HomeController : Controller
    {

        
        public ActionResult Index()
        {
            return View();
        }
        [Route("about")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
       [Route("contact")]
        public ActionResult Contact()
        {
            return View();
        }

        [Route("contact")]
        [HttpPost]
        [CaptchaValidator]
        public ActionResult Contact(ContactUsModel model, bool captchaValid)
        {
            if (ModelState.IsValid && captchaValid)
            {

                var sb = new StringBuilder();
                sb.Append("Name: " + model.FirstName + " " + model.LastName+"<br>");
                sb.Append("Mobile: " + model.Mobile + "<br>");
                sb.Append("Email: " + model.Email + "<br>");
                sb.Append("Message: " + model.Message + "<br><br>");
                var subject = "New message gaurik systems from contact us form";
                var adminEmail = ConfigurationManager.AppSettings["AdminMail"];

                EmailHelper.SendEmail(adminEmail, subject, sb.ToString(), model.FirstName + " " + model.LastName, model.Email);
                ViewBag.Message = "Your message has been sent";


                var s = new StringBuilder();
                s.Append("Hello " + model.FirstName + "<br><br>");
                s.Append("Your message has been sent<br><br>");
                s.Append("Gaurik Systems");
                EmailHelper.SendEmail(model.Email, "Your message has been sent- gaurik systems", s.ToString(), "Gaurik System", adminEmail);
                ViewBag.Message = "Your message has been sent";

                return View();
            }

            return View(model);
        }

        [Route("quote")]
        public ActionResult Quote()
        {
            var model = new QuoteModel();
           // model.IsPersonalOwnership = true;
          //  model.Starttime = DateTime.Now;
            return View(model);
        }

        [Route("quote")]
        [HttpPost]
        [CaptchaValidator]
        public ActionResult Quote(QuoteModel model, bool captchaValid)
        {
            if (ModelState.IsValid && captchaValid)
            {
                var sb = new StringBuilder();
                sb.Append("Name: " + model.FirstName + " " + model.LastName + "<br>");
                sb.Append("Mobile: " + model.Mobile + "<br>");
                sb.Append("Email: " + model.Email + "<br>");
                sb.Append("Address: " + model.Address + "  "+ model.State +" "+ model.Country+"<br>");
                sb.Append("Services interested" + model.Services + "<br>");
                sb.Append("WebiteType" + model.WebiteType + "<br>");
                if(model.Starttime.HasValue)
                sb.Append("Start time" + model.Starttime + "<br>");
                var ownership = "Personal";
                if (model.IsPersonalOwnership!=true)
                    ownership = "Corporate";
                sb.Append("Ownership " + ownership + "<br>");

                var domainRequired = model.DomainRequired==true ? "Yes" : "NO";
                sb.Append("Domain Required? " + domainRequired + "<br>");

                var hostingRequired = model.HostingRequired == true ? "Yes" : "NO";
                sb.Append("Domain Required? " + hostingRequired + "<br>");

                sb.Append("Message: " + model.Message + "<br><br>");
                var subject = "New message from gaurik systems quote form";
                var adminEmail = ConfigurationManager.AppSettings["AdminMail"];
                //Admin Message
                EmailHelper.SendEmail(adminEmail, subject, sb.ToString(), model.FirstName + " " + model.LastName, model.Email);
                //Message to clients
                var s = new StringBuilder();
                s.Append("Hello " + model.FirstName + "<br><br>");
                s.Append("Your message has been sent<br><br>");
                s.Append("Gaurik Systems");
                EmailHelper.SendEmail(model.Email, "Your message has been sent- gaurik systems", s.ToString(), "Gaurik System", adminEmail);
                ViewBag.Message = "Your message has been sent";
                return View(model);
            }
            return View(model);
        }

        [Route("portfolio")]
        public ActionResult Portfolio()
        {
            return View();
        }
        [Route("blog")]
        public ActionResult Blog()
        {
            return View();
        }
        [Route("blog/{seoName}")]
        public ActionResult BlogDetail(string seoName)
        {

            return View();
        }
        [Route("services")]
        public ActionResult Service()
        {
            return View("services");
        }
        [Route("services/{id}")]
        public ActionResult Services(string id)
        {
            if(string.IsNullOrEmpty(id))
            return View();
            id = id.Replace("-", "");
            return View(id);
        }
        //[Authorize]
        //[Route("pay")]
        //public ActionResult Pay()
        //{
        //    return View();
        //}
    }
}