﻿using GaurikSystem.Models;
using GaurikSystem.Provider.AuthorizeNet;
using System;
using System.Web.Mvc;

namespace GaurikSystem.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        private readonly AuthorizeNetPaymentProcessor _authorizeNetPaymentProcessor;
        public PaymentController()
        {
            _authorizeNetPaymentProcessor = new AuthorizeNetPaymentProcessor();

        }
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }
        //[Authorize]
       [Route("pay")]
        public ActionResult Pay()
        {
            
            return View(new BillPaymentCardDetailModel());
        }
       // [Authorize]
        [HttpPost]
        [Route("pay")]
        public ActionResult Pay(BillPaymentCardDetailModel model)
        {
            if (model.OrderGuid == null)
                model.OrderGuid = Guid.NewGuid().ToString();
            if (ModelState.IsValid)
            {
                var payment = _authorizeNetPaymentProcessor.ProcessPayment(model);
                if (payment.Success)
                {
                    //To-do Send Email
                    return View("PaymentSuccess");
                }
                else
                {
                    foreach(var error in payment.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

            }
            
            return View(model);
        }
    }
}